# Search Engine

Proiectul va oferi posibilitatea utilizatorului de a căuta după o serie de cuvinte cheie resurse disponibile pe internet. Suplimentar acesta va oferi o manieră "inteligentă" de a filtra conținutul dorit folosind o căutare multicriterială încercând să ofere conținut cât mai relevant.

Arhitectura proiectului

- compontenta crawler
    - componenta curentă se va ocupa de descoperirea resurselor ce ar putea să fie de interes pentru motorul de căutare ce urmează să fie implementat. Acesta va pleca de la o serie de resurse cunoscute (de exemplu [topul alexa](https://www.alexa.com/topsites) pentru cele mai populare website-uri) urmând ca apoi să descopere resurse / domenii noi pe care să le descarce.
    - crawler-ul va ține cont de restricțiile pentru roboți existente în fișierul robots.txt și eventual de restricțiile la nivel de url (de exemplu: nofollow).
    - conținutul paginilor web descoperite vor fi salvate pe disk într-o structură ierarhică iar metadatale despre paginile respective vor fi salvate într-o bază de date (de preferat o bază de date nerelațională de exemplu Redis).

- componenta procesator
    - această componentă se va ocupa de procesarea conținutului descărcat și va încerca să obțină cât mai multe informații ce vor putea fi folosite de componenta de căutare, de exemplu:
        - cuvinte cheie
        - descrierea paginii
        - autorii
        - cele mai folosite cuvinte
    - toate informațiile și tag-urile obținute pentru fiecare pagină vor fi stocate într-o bază de date (preferabil aceeași ca cea folosită de crawler).

- componenta motor de căutare:
    - pe baza informațiilor obținute de procesator această componentă va încerca să găsească cele mai relevante resurse web unde s-au regătit termenii și/sau filterele introduse de utilizator pentru căutare.
    - de asemenea se va folosi de o metodă de a detecta typos în input-ul de la utilizator (de exemplu se poate folosi distanța Levenshtein pentru a-i oferi sugestii).
